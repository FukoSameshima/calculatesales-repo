package jp.alhinc.sameshima_fuko.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class CalculateSales {

	public static void main(String[] args) {

		//【エラー処理3-1】コマンドライン引数が渡されているか確認
		if (args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		Map<String, String> branchNames = new HashMap<>();
		Map<String, Long> branchSales = new HashMap<>();

		if(!input(args[0], "branch.lst", branchNames, branchSales, "^[0-9]{3}$", "支店")) {
		return;
		}

		//2-1

		/*listFiles を使用して files という配列に指定したパスに存在する
		全てのファイルの情報が格納*/
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		//指定したパスに存在する全てのファイルの数だけ繰り返し
		for(int i = 0; i < files.length ; i++) {

			//ファイル名を取得
			String fileName = files[i].getName();


			//matches を使用してファイル名が「数字8桁.rcd」なのか判定
			//【エラー処理3】isFileを使用して、対象がファイルであり「数字8桁.rcd」なのか判定
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
		}
	}
		//【エラー処理2-1】ファイル名が連番になっているかの確認

		//ファイルをソートして名前順に並び替え
		Collections.sort(rcdFiles);

		//売上ファイル-1の数だけ繰り返す
		for(int i = 0; i < rcdFiles.size() - 1; i++) {

			//比較する2つのファイル名の先頭から数字の8文字を切り出し、int型に変換
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			//2つのファイル名の数字を比較して、差が1ではなかったらエラー
			if((latter - former) != 1) {
				System.out.println("売上ファイルが連番になっていません");
				return;
			}
		}

		//2-2

		BufferedReader br = null;
		//売上ファイルの数だけ読み込み
		for(int i = 0; i < rcdFiles.size(); i++) {
			try {

				//売上ファイル一行ずつデータを読み込み
				FileReader filereader = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(filereader);

				String readLine;
				ArrayList<String> readArray = new ArrayList<String>();

				while ((readLine = br.readLine()) != null) {
					//リストを読み込む
					readArray.add(readLine);
				}

				//【エラー処理2-2】売上ファイルの内容がちゃんと2行か確認
				if(readArray.size() != 2) {
					System.out.println(rcdFiles.get(i).getName() + "のフォーマットが不正です");
					return;
				}

				//変数readCodeに支店コード0
				String readCode = readArray.get(0);
				//変数readCodeに売上額1
				String readSale = readArray.get(1);

				//【エラー処理3】売上金額が数字なのか確認
				if(!readSale.matches("^[0-9]*$")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//【エラー処理2-2】Mapに特定のKeyが存在するか確認
				if (!branchNames.containsKey(readCode)) {
					System.out.println(rcdFiles.get(i).getName() + "の支店コードが不正です");
					return;
				}

				//ファイルから売上額readSaleを抽出
				Long salesAmount = branchSales.get(readCode);
				//long型に変換した売上額を加算して売上合計にする
				salesAmount += Long.parseLong(readSale);

				//【エラー処理2-2】売上金額の合計が10桁を超えていないか確認

				if(salesAmount >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				branchSales.put(readCode, salesAmount);

			}catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			}finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!output(args[0], "blanch.out", branchNames, branchSales)) {
		return;
		}
	}

	public static boolean input(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String number, String branch) {

		BufferedReader br = null;
		try {
			File file = new File(path, fileName);

			//【エラー処理1-1】そもそもファイルが存在するかの確認
			if(!file.exists()) {
				System.out.println(branch + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			//1-2

			String line;
			//支店定義ファイルを一行ずつデータを読み込み
			while((line = br.readLine()) != null) {
				String[] items = line.split(",");

				//【エラー処理1-2】配列の要素数、数字3桁のチェック
				if((items.length != 2) || (!items[0].matches(number))){
					System.out.println(branch + "定義ファイルのフォーマットが不正です");
					return false;
				}

				//全ての[0]支店コードとそれに対応する[1]支店名を保持
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		}catch(IOException e) {    //例外が発生した場合（定義されていない内容のエラー）
			System.out.println("予期せぬエラーが発生しました");
			return false;
			//例外の有無に関わらず、最後に必ず実行される処理
		}finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	return true;
	}


	public static boolean output(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {

		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//Mapから全てのKeyを取得
			for (String key : names.keySet()) {

				//ファイルに支店名と支店コードと売り上げを書き込む
				bw.write(key + "," + names.get(key) + "," + sales.get(key));

				//改行
				bw.newLine();

			}
		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}

		}
		return true;
	}
}


